#文档说明
这是基于OAuth2，用java封装了qq、新浪微博、微信的第三方登录的接口
#架构
com.shamrock.oauth.api.entity =>第三方登录后用户信息实体
com.shamrock.oauth.api.httpclient =>采用httpcient封装网络层，有一个彩蛋哦,这里可以用于爬虫，非常方便
com.shamrock.oauth.api.service => 对qq、新浪微博、微信第三方登录的接口定义和封装
com.shamrock.oauth.util =>读取配置文件和json的解析
conf  =>配置文件
lib => 所需jar包
#示例
package com.shamrock.oauth.api.service;
public class Main {
	public static void main(String[] args) {
		OAuthApiService qq = new QQOAuthApi();
		//qq登录授权
		System.out.println(qq.authorize());
		//授权后获取用户信息
		//qq.getUser(code);
		OAuthApiService sina = new SinaWeiboOAuthApi();
		//新浪微博登录授权
		System.out.println(sina.authorize());
		//授权后获取用户信息
		//sina.getUser(code);
		OAuthApiService weixin = new WeiXinOAuthApi();
		//微信登录授权
		System.out.println(weixin.authorize());
		//授权后获取用户信息
		//weixin.getUser(code);
		
	}
}


